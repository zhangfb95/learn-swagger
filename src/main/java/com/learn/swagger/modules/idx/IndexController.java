package com.learn.swagger.modules.idx;

import com.learn.swagger.modules.little.service.SendService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Date;

/**
 * @author zhangfb
 */
@Api(value = "index", tags = "index")
@Slf4j
@RequestMapping
@Controller
public class IndexController {

    private Date startupTime = new Date();

    @Autowired
    private SendService sendService;

    @ApiOperation(value = "首页信息", notes = "首页信息")
    @RequestMapping(value = {"", "/"}, method = RequestMethod.GET)
    public String index(Model model) {
        model.addAttribute("startupTime", startupTime);
        return "index";
    }

    @ApiOperation(value = "注册", notes = "注册用户，参考[链接](http://www.baidu.com)")
    @RequestMapping(value = {"send"}, method = RequestMethod.POST)
    public String send(@ModelAttribute("msg") final String msg) {
        sendService.call(msg);
        return "send";
    }
}
