package com.learn.swagger.modules.little.service;

/**
 * @author zhangfb
 */
public interface SendService {

    void call(String msg);

    void call();
}
