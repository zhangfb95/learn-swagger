package com.learn.swagger.modules.little.service.impl;

import com.learn.swagger.modules.little.service.SendService;
import org.springframework.stereotype.Service;

/**
 * @author zhangfb
 */
@Service("isSendService")
public class SendServiceImpl implements SendService {

    @Override
    public void call(String msg) {
        System.out.println(msg);
    }

    @Override
    public void call() {
        System.out.println("sb");
    }
}
