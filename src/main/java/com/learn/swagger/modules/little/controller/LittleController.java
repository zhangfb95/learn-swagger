package com.learn.swagger.modules.little.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author zhangfb
 */
@Api(value = "little", tags = "little")
@Slf4j
@RequestMapping("little")
@Controller
public class LittleController {

    @ApiOperation(value = "点点small")
    @RequestMapping(value = {"smallTalk"}, method = RequestMethod.GET)
    public String smallTalk(Model model) {
        return "index";
    }
}
