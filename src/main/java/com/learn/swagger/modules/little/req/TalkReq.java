package com.learn.swagger.modules.little.req;

import lombok.Data;

/**
 * @author zhangfb
 */
@Data
public class TalkReq {

    private Long id;
    private String name;
}
