package com.learn.swagger.modules.little.controller;

import com.learn.swagger.base.bio.Request;
import com.learn.swagger.modules.little.req.TalkReq;
import com.learn.swagger.modules.little.res.TalkRes;
import com.learn.swagger.modules.little.service.SendService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author zhangfb
 */
@Api(value = "little", tags = "little")
@Slf4j
@RequestMapping("little")
@Controller
public class Little2Controller {

    @Autowired
    private SendService sendService;

    @ApiOperation(value = "点点")
    @RequestMapping(value = {"bigTalk"}, method = RequestMethod.GET)
    public String bigTalk(Model model) {
        sendService.call();
        return "index";
    }

    @ApiOperation(value = "点点2")
    @ResponseBody
    @RequestMapping(value = {"bigTalk"}, method = RequestMethod.POST)
    public TalkRes talk(@RequestBody Request<TalkReq> request) {
        return new TalkRes();
    }
}
