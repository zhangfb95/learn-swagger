package com.learn.swagger.modules.little.res;

import lombok.Data;

/**
 * @author zhangfb
 */
@Data
public class TalkRes {

    private Long id;
}
