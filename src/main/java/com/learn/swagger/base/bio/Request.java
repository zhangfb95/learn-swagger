package com.learn.swagger.base.bio;

import lombok.Data;

/**
 * @author zhangfb
 */
@Data
public class Request<T> {

    private T content;
}
